import os
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup

url = raw_input("Spider target: ")
os.system("mkdir -p tmp")
os.chdir("tmp")
os.system("wget " + url)
os.system("find -exec cat {} \; | cat >> ../sum.txt")
os.chdir("../")

fi = open("sum.txt",'r')
content = fi.read()
soup = BeautifulSoup(content)
fi.close()
os.system("rm -rf tmp")
os.system("rm sum.txt")
wordcloud = WordCloud().generate(soup.get_text())
plt.imshow(wordcloud)
plt.axis("off")
plt.show()
